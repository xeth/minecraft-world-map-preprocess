Minecraft world map height map pre-processing for world painter.


## Folder Structure
```
build/       - generated stuff
data/        - raw world map data
src/         - python processing source
scripts/     - public run scripts
```
